#    Leitner revision tool
#    Copyright (C) 2020 hackintosh5

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.

#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import cmd
import base64
import datetime
import functools
import inspect
import pickle
import pprint
import readline

from typing import Sequence

from . import models, patterns, calendars


class TUI(cmd.Cmd):
    prompt = "> "

    def __init__(self, terms: Sequence[models.Term]):
        super().__init__()
        self._terms = terms
        self._session = None

    @functools.wraps(cmd.Cmd.do_help)
    def do_help(self, cmdline: str):
        split = cmdline.split()
        doc = None
        if len(split) == 1:
            func = getattr(self, "do_" + split[0], None)
            if func:
                doc = inspect.getdoc(func)
        if doc:
            print(doc)
        else:
            return super().do_help(cmdline)

    def do_EOF(self, cmdline: str):
        """
        Exit the program
        """
        print()
        return True

    def do_quit(self, cmdline: str):
        """
        Exit the program
        """
        return True

    def do_exit(self, cmdline: str):
        """
        Exit the program
        """
        return True

    def do__dump(self, cmdline: str):
        """
        Output debugging information
        """
        print(self._terms)
        print()
        print(self._session)
        print()
        print(base64.b64encode(pickle.dumps(self)).decode())

    def do__reset_levels(self, cmdline: str):
        """
        Resets all terms to the given level
        Usage: _reset_levels level
        """
        level = int(cmdline)
        for term in self._terms:
            term.level = level

    def do__calendar(self, cmdline: str):
        """
        Pretty-print the revision schedule
        """
        now = datetime.datetime.now()
        today = now.date()
        calendar = calendars.TermCalendar(self._terms).organize(today)
        pprint.pp(calendar)

    def do_end(self, cmdline: str):
        """
        End the current study session
        """
        self._session = None

    def do_add(self, cmdline: str):
        """
        Add a new term for revision.
        Usage: add [question [| answer]]
        """
        split = [entry.strip() for entry in cmdline.split("|") if entry and not entry.isspace()]
        question, answer = None, None
        if len(split) >= 1:
            question = split[0]
        if len(split) >= 2:
            answer = split[1]
        if question is None:
            question = input("Enter the question:\n> ").strip()
        if answer is None:
            answer = input("Enter the answer\n> ").strip()
        term = models.LeitnerTerm(models.QuestionAnswerTerm(question, answer), datetime.datetime.min, 0)
        self._terms.append(term)

    def do_del(self, cmdline: str):
        """
        Remove a term
        Usage: del [question]
        """
        if self._session:
            print("Terms cannot be deleted during an ongoing session.")
            return
        if cmdline:
            question = cmdline
        else:
            question = input("Enter the question:\n> ")
        question = question.strip()
        self._terms = [term for term in self._terms if term.term.question != question]

    def do_edit(self, cmdline: str):
        """
        Edit an existing term
        Usage: edit [current_question [| new_question [| new_answer]]]
        """
        split = [entry.strip() for entry in cmdline.split("|") if entry and not entry.isspace()]
        init_question, new_question, new_answer = None, None, None
        if len(split) >= 1:
            init_question = split[0]
        if len(split) >= 2:
            new_question = split[1]
        if len(split) >= 3:
            new_answer = split[2]
        if init_question is None:
            init_question = input("Enter the initial question:\n> ").strip()
        if new_question is None:
            new_question = input("Enter the new question:\n> ").strip()
        if new_answer is None:
            new_answer = input("Enter the new answer:\n> ").strip()
        count = 0
        for term in self._terms:
            if term.term.question == init_question:
                term.term.question = new_question
                term.term.answer = new_answer
                count += 1
        print(f"Edited {count} items")

    def do_quiz(self, cmdline: str):
        """
        Start a new quiz, or continue any existing quiz.
        """
        now = datetime.datetime.now()
        today = now.date()
        if self._session and self._session.date.date() == today:
            return self._run_session()
        calendar = calendars.TermCalendar(self._terms).organize(today)
        try:
            terms_for_today = calendar[today]
        except KeyError:
            print("Nothing left to learn today! Have fun :P")
            return False
        self._session = models.StudySession(terms_for_today, now)
        return self._run_session()

    def _run_session(self):
        for term in self._session.terms[self._session.studied_to_index:]:
            if self._run_term(term.term):
                term.last_studied = self._session.date
                if term.level + 1 < len(models.FREQUENCIES):
                    term.level += 1
            else:
                if term.level > 0:
                    term.level -= 1
        self._session = None

    def _run_term(self, term):
        if isinstance(term, models.QuestionAnswerTerm):
            history_index = readline.get_current_history_length()
            given_answer = input("What is the answer for " + term.question + "?\n> ")
            try:
                readline.remove_history_item(history_index)
            except ValueError:
                pass
            print("\033[F> " + " " * len(given_answer))  # Blank previous line
            if not patterns.pattern_matches(given_answer, term.answer):
                while not patterns.pattern_matches(given_answer, term.answer):
                    print("Wrong. The correct answer was " + term.answer)
                    input("Press return when you're ready to continue.")
                    print("\033[F\033[FWrong. The correct answer is now hidden."
                          + " " * (len(term.answer) - 10) + "\nType the correct answer:" + " " * 19)
                    history_index = readline.get_current_history_length()
                    given_answer = input("> ")
                    try:
                        readline.remove_history_item(history_index)
                    except ValueError:
                        pass
                    print("\033[F> " + " " * len(given_answer))  # Blank previous line
                return False
            else:
                print("Well done!")
                return True

    def __getstate__(self):
        return {"_terms": self._terms, "_session": self._session}

    def __setstate__(self, state):
        super().__init__()
        self.__dict__.update(state)
