#    Leitner revision tool
#    Copyright (C) 2020 hackintosh5

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.

#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime

from abc import ABC
from dataclasses import dataclass
from typing import Sequence, Iterable


# How often the given Leitner level needs to be studied
FREQUENCIES = (
    (0, 0),
    (1, 1),
    (3, 4),
    (5, 8),
    (12, 16),
    (28, 31),
)


class Term(ABC):
    pass


@dataclass
class QuestionAnswerTerm(Term):
    question: str
    answer: str


@dataclass
class LeitnerTerm:
    term: Term
    last_studied: datetime.datetime
    level: int

    def next_study_range(self, frequencies) -> Iterable[int]:
        start, end = frequencies[self.level]
        return (self.last_studied.date() + datetime.timedelta(days=days) for days in range(start, end + 1))


@dataclass
class StudySession:
    terms: Sequence[Term]
    date: datetime.date
    studied_to_index: int = 0


class PatternComponent:
    def __init__(self):
        self.active: bool = False

    def descend(self, component):
        raise TypeError(f"Cannot descend into {type(self)}")

    def ascend(self):
        raise TypeError(f"Cannot ascend from {type(self)}")

    def append(self):
        raise TypeError(f"Cannot append to {type(self)}")

    def match(self):
        return True

    def __bool__(self):
        return True
