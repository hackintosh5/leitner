from typing import Collection

from .models import PatternComponent


class BasePatternComponent(PatternComponent):
    def __init__(self):
        super().__init__()
        self.components: Collection[PatternComponent] = []

    def descend(self, component: PatternComponent):
        if self.active:
            assert isinstance(component, self.valid_components)
            self.components.append(component)
            component.active = True
            self.active = False
        else:
            self.components[-1].descend(component)

    def ascend(self):
        if self.components[-1].active:
            self.components[-1].active = False
            self.active = True
        else:
            self.components[-1].ascend()

    def append(self, *args, **kwargs):
        if self.active:
            raise NotImplementedError()
        else:
            self.components[-1].append(*args, **kwargs)

    def match(self, sibling, so_far, *args, **kwargs) -> int:
        ret = 0
        i = 0
        for component in self.components:
            this_ret = component.match(i, so_far + ret, *args, **kwargs)
            if this_ret is False:
                return False
            ret += this_ret
        return ret

    def __bool__(self):
        return any(self.components)

    def __repr__(self):
        return f"{type(self).__name__}(children={repr(self.components)})"


class RootPatternComponent(BasePatternComponent):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.valid_components = BASIC_PATTERN_COMPONENTS
        self.active = True

    def match(self, *args, **kwargs):
        return super().match(0, 0, *args, **kwargs)


class OptionalPatternComponent(BasePatternComponent):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.valid_components = BASIC_PATTERN_COMPONENTS

    def match(self, sibling, so_far, *args, **kwargs) -> int:
        ret = 0
        i = 0
        for component in self.components:
            this_ret = component.match(i, so_far + ret, *args, **kwargs)
            if this_ret is False:
                return 0
            ret += this_ret
        return ret


class MutuallyExclusiveChoicesPatternComponent(BasePatternComponent):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.valid_components = (MutuallyExclusiveChoicePatternComponent,)

    def match(self, sibling, so_far, *args, **kwargs) -> int:
        ret = 0
        i = 0
        for component in self.components:
            this_ret = component.match(i, so_far + ret, *args, **kwargs)
            if this_ret is not False:
                return this_ret
            ret += this_ret
        return False


class MutuallyExclusiveChoicePatternComponent(BasePatternComponent):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.valid_components = BASIC_PATTERN_COMPONENTS


class WordPatternComponent(PatternComponent):
    def __init__(self):
        super().__init__()
        self.word = ""

    def append(self, data: str):
        self.word += data

    def match(self, sibling, so_far, text):
        if not self.word:
            return 0
        word = ""
        i = 0
        for char in text[so_far:]:
            if not char.isalnum():
                if word:
                    break
                else:
                    i += 1
                    continue
            word += char
            i += 1
        return i if word.strip() == self.word else False

    def __repr__(self):
        return f"{type(self).__name__}(word={self.word})"


class EndPatternComponent(PatternComponent):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def match(self, sibling, so_far, text):
        space = text[so_far:]
        matches = all(not char.isalnum() for char in space)
        return 0 if matches else False

    def __repr__(self):
        return f"{type(self).__name__}()"


BASIC_PATTERN_COMPONENTS = (OptionalPatternComponent,
                            MutuallyExclusiveChoicesPatternComponent,
                            WordPatternComponent,
                            EndPatternComponent)


class Pattern:
    def __init__(self, pattern):
        self.component = self._parse(pattern)

    @classmethod
    def _parse(cls, pattern):
        component = RootPatternComponent()
        component.descend(WordPatternComponent())
        for char in pattern:
            if char == "[":
                component.ascend()
                component.descend(OptionalPatternComponent())
                component.descend(WordPatternComponent())
            elif char == "]":
                component.ascend()
            elif char == "{":
                component.ascend()
                component.descend(MutuallyExclusiveChoicesPatternComponent())
                component.descend(MutuallyExclusiveChoicePatternComponent())
                component.descend(WordPatternComponent())
            elif char == "/":
                component.ascend()
                component.ascend()
                component.descend(MutuallyExclusiveChoicePatternComponent())
                component.descend(WordPatternComponent())
            elif char == "}":
                component.ascend()
                component.ascend()
                component.ascend()
                component.descend(WordPatternComponent())
            elif char.isalnum():
                component.append(char)
            else:
                component.ascend()
                component.descend(WordPatternComponent())
        component.ascend()
        component.descend(EndPatternComponent())
        return component

    def match(self, actual):
        return self.component.match(actual)


def pattern_matches(actual, pattern):
    return Pattern(pattern).match(actual)
