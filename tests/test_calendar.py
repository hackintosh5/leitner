#    Leitner revision tool
#    Copyright (C) 2020 hackintosh5

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.

#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime

from ..calendars import TermCalendar
from ..models import LeitnerTerm, QuestionAnswerTerm


BASE_DATETIME = datetime.datetime(year=2020, month=9, day=17, hour=0, minute=0, second=0)
BASE_DATE = BASE_DATETIME.date()
TODAY_DATE = BASE_DATE - datetime.timedelta(days=1)

TEST_FREQUENCIES = [
    (1, 2),
    (3, 4),
    (5, 8),
    (12, 16),
    (28, 31),
]


def test_calendar_one_term_one_day():
    terms = [LeitnerTerm(QuestionAnswerTerm("q", "a0"), BASE_DATETIME, 0)]
    calendar = TermCalendar(terms, TEST_FREQUENCIES)
    result = calendar.organize(BASE_DATE)
    assert result == {BASE_DATE + datetime.timedelta(days=1): terms}


def test_calendar_two_terms_one_day():
    terms = [
        LeitnerTerm(QuestionAnswerTerm("q", "a0"), BASE_DATETIME, 0),
        LeitnerTerm(QuestionAnswerTerm("q", "a1"), BASE_DATETIME, 0)
    ]
    calendar = TermCalendar(terms, TEST_FREQUENCIES)
    result = calendar.organize(BASE_DATE)
    assert result == {
        BASE_DATE + datetime.timedelta(days=1): [terms[0]],
        BASE_DATE + datetime.timedelta(days=2): [terms[1]]
    }


def test_calendar_two_terms_two_days():
    terms = [
        LeitnerTerm(QuestionAnswerTerm("q", "a0"), BASE_DATETIME, 0),
        LeitnerTerm(QuestionAnswerTerm("q", "a1"), BASE_DATETIME + datetime.timedelta(days=1), 0)
    ]
    calendar = TermCalendar(terms, TEST_FREQUENCIES)
    result = calendar.organize(BASE_DATE)
    assert result == {
        BASE_DATE + datetime.timedelta(days=1): [terms[0]],
        BASE_DATE + datetime.timedelta(days=2): [terms[1]]
    }


def test_calendar_two_terms_two_days_split():
    terms = [
        LeitnerTerm(QuestionAnswerTerm("q", "a0"), BASE_DATETIME, 0),
        LeitnerTerm(QuestionAnswerTerm("q", "a1"), BASE_DATETIME + datetime.timedelta(days=2), 0)
    ]
    calendar = TermCalendar(terms, TEST_FREQUENCIES)
    result = calendar.organize(BASE_DATE)
    assert result == {
        BASE_DATE + datetime.timedelta(days=1): [terms[0]],
        BASE_DATE + datetime.timedelta(days=3): [terms[1]]
    }


def test_calendar_three_terms_one_day():
    terms = [
        LeitnerTerm(QuestionAnswerTerm("q", "a0"), BASE_DATETIME, 0),
        LeitnerTerm(QuestionAnswerTerm("q", "a1"), BASE_DATETIME, 0),
        LeitnerTerm(QuestionAnswerTerm("q", "a2"), BASE_DATETIME, 0)
    ]
    calendar = TermCalendar(terms, TEST_FREQUENCIES)
    result = calendar.organize(BASE_DATE)
    assert result == {
        BASE_DATE + datetime.timedelta(days=1): [terms[0], terms[2]],
        BASE_DATE + datetime.timedelta(days=2): [terms[1]]
    }


def test_calendar_three_terms_two_days():
    terms = [
        LeitnerTerm(QuestionAnswerTerm("q", "a0"), BASE_DATETIME, 0),
        LeitnerTerm(QuestionAnswerTerm("q", "a1"), BASE_DATETIME, 0),
        LeitnerTerm(QuestionAnswerTerm("q", "a2"), BASE_DATETIME + datetime.timedelta(days=1), 0)
    ]
    calendar = TermCalendar(terms, TEST_FREQUENCIES)
    result = calendar.organize(BASE_DATE)
    assert result == {
        BASE_DATE + datetime.timedelta(days=1): [terms[0]],
        BASE_DATE + datetime.timedelta(days=2): [terms[1]],
        BASE_DATE + datetime.timedelta(days=3): [terms[2]]
    }


def test_calendar_three_terms_three_days():
    terms = [
        LeitnerTerm(QuestionAnswerTerm("q", "a0"), BASE_DATETIME, 0),
        LeitnerTerm(QuestionAnswerTerm("q", "a1"), BASE_DATETIME + datetime.timedelta(days=1), 0),
        LeitnerTerm(QuestionAnswerTerm("q", "a2"), BASE_DATETIME + datetime.timedelta(days=2), 0)
    ]
    calendar = TermCalendar(terms, TEST_FREQUENCIES)
    result = calendar.organize(BASE_DATE)
    assert result == {
        BASE_DATE + datetime.timedelta(days=1): [terms[0]],
        BASE_DATE + datetime.timedelta(days=2): [terms[1]],
        BASE_DATE + datetime.timedelta(days=3): [terms[2]]
    }


def test_calendar_three_terms_mixed_levels_one_day():
    terms = [
        LeitnerTerm(QuestionAnswerTerm("q", "a0"), BASE_DATETIME, 1),
        LeitnerTerm(QuestionAnswerTerm("q", "a1"), BASE_DATETIME, 1),
        LeitnerTerm(QuestionAnswerTerm("q", "a2"), BASE_DATETIME, 0)
    ]
    calendar = TermCalendar(terms, TEST_FREQUENCIES)
    result = calendar.organize(BASE_DATE)
    assert result == {
        BASE_DATE + datetime.timedelta(days=3): [terms[0]],
        BASE_DATE + datetime.timedelta(days=4): [terms[1]],
        BASE_DATE + datetime.timedelta(days=1): [terms[2]]
    }


def test_calendar_three_terms_mixed_levels_tricky_days():
    terms = [
        LeitnerTerm(QuestionAnswerTerm("q", "a0"), BASE_DATETIME, 0),
        LeitnerTerm(QuestionAnswerTerm("q", "a1"), BASE_DATETIME + datetime.timedelta(days=1), 0),
        LeitnerTerm(QuestionAnswerTerm("q", "a2"), BASE_DATETIME + datetime.timedelta(days=1), 0),
        LeitnerTerm(QuestionAnswerTerm("q", "a3"), BASE_DATETIME + datetime.timedelta(days=2), 0),
        LeitnerTerm(QuestionAnswerTerm("q", "a4"), BASE_DATETIME - datetime.timedelta(days=1), 2),
        LeitnerTerm(QuestionAnswerTerm("q", "a5"), BASE_DATETIME - datetime.timedelta(days=10), 3)
    ]
    calendar = TermCalendar(terms, TEST_FREQUENCIES)
    result = calendar.organize(BASE_DATE)
    assert result == {
        BASE_DATE + datetime.timedelta(days=1): [terms[0]],
        BASE_DATE + datetime.timedelta(days=2): [terms[1]],
        BASE_DATE + datetime.timedelta(days=3): [terms[2]],
        BASE_DATE + datetime.timedelta(days=4): [terms[3]],
        BASE_DATE + datetime.timedelta(days=5): [terms[4]],
        BASE_DATE + datetime.timedelta(days=6): [terms[5]],
    }
