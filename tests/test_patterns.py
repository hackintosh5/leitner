#    Leitner revision tool
#    Copyright (C) 2020 hackintosh5

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.

#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

from ..patterns import pattern_matches


def test_pattern_matches_simple():
    assert pattern_matches("hello world. goodbye world!", "hello world. goodbye world!")


def test_pattern_matches_punctuation():
    assert pattern_matches("hello world. goodbye world!", "hello world goodbye world")


def test_pattern_matches_punctuation_complex():
    assert pattern_matches("hello   world'''# goodbye.!!\"£$%^&*()@:[]{} world", "hello world. goodbye world!")


def test_pattern_matches_optional():
    assert pattern_matches("hello world goodbye", "hello world goodbye[ world]")
    assert pattern_matches("hello world goodbye world", "hello world goodbye[ world]")


def test_pattern_matches_mutually_exclusive():
    assert pattern_matches("hello world hello world", "hello world{ hello / goodbye }world")
    assert pattern_matches("hello world goodbye world", "hello world{ hello / goodbye }world")


# negative tests


def test_pattern_not_matches_simple():
    assert not pattern_matches("hello worldgoodbye world!", "hello world. goodbye world!")


def test_pattern_not_matches_extra():
    assert not pattern_matches("hello world. goodbye world!", "hello world. goodbye!")


def test_pattern_not_matches_optional():
    assert not pattern_matches("hello goodbye", "hello world goodbye[ world]")
    assert not pattern_matches("hello world world ", "hello world goodbye[ world]")


def test_pattern_not_matches_mutually_exclusive():
    assert not pattern_matches("hello world hellish world", "hello world{ hello / goodbye }world")
    assert not pattern_matches("hello world goodbye cruel world", "hello world{ hello / goodbye }world")
