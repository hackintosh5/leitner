# Leitner revision tool

This program helps with learning MFLs by keeping track of how well you know each word using Leitner boxes.

<p><a href="https://commons.wikimedia.org/wiki/File:Leitner_system.svg#/media/File:Leitner_system.svg"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c4/Leitner_system.svg/1200px-Leitner_system.svg.png" alt="Leitner system.svg"></a><br>Illustration by <a href="//commons.wikimedia.org/wiki/User:Zirguezi" title="User:Zirguezi">Zirguezi</a> - <span class="int-own-work" lang="en">Own work</span>, <a href="http://creativecommons.org/publicdomain/zero/1.0/deed.en" title="Creative Commons Zero, Public Domain Dedication">CC0</a>, <a href="https://commons.wikimedia.org/w/index.php?curid=20328017">Link</a></p>