import collections
import datetime

from typing import Sequence, Dict

from .models import FREQUENCIES, LeitnerTerm


class TermCalendar:
    def __init__(self, terms: Sequence[LeitnerTerm], frequencies: Sequence[Sequence[int]] = FREQUENCIES):
        self.terms = terms
        self.frequencies = frequencies

    def _sort_key(self, term):
        """
        Return sorting value for term according to the rules:
        - Shortest applicable range first
        - Then sort by first applicable (ascending start of study range)
        """
        start, end = self.frequencies[term.level]
        return (end - start, term.last_studied.date() + datetime.timedelta(days=start))

    def organize(self, first_day) -> Dict[datetime.date, LeitnerTerm]:
        terms_by_day = collections.defaultdict(list)
        for term in sorted(self.terms, key=self._sort_key):
            best_day = float("inf"), datetime.date.min
            for day in term.next_study_range(self.frequencies):
                if day < first_day:
                    continue
                terms = terms_by_day[day]
                terms_len = len(terms)
                if terms_len < best_day[0]:
                    best_day = terms_len, day
            if best_day[0] == float("inf"):
                # all possible study dates are in the past, reassign ASAP
                best_day = None, first_day
            terms_by_day[best_day[1]].append(term)
        return {k: v for k, v in terms_by_day.items() if v}
